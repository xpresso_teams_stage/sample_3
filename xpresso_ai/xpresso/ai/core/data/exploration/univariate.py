from xpresso.ai.core.data.exploration import Explorer
from xpresso.ai.core.data.exploration import Understander
from sklearn.base import BaseEstimator

class UnivariateExplorer(BaseEstimator):
    """
    Performs univariate analytis on the Structured Dataset object
    """
    def fit(self,dataset):
        """
        Performs univariate analytis on the dataset
        Args:
            dataset (StructuredDataset): StructuredDataset object
        """

        if not len(dataset.info.attributeInfo):
            Understander().fit(dataset)
        explorer = Explorer(dataset)
        explorer.explore_univariate()
